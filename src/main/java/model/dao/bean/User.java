package model.dao.bean;

public class User {
	private int IdUser;
	private String login;
	private String password;
	public int getIdUser() {
		return IdUser;
	}
	public void setIdUser(int id) {
		IdUser = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
