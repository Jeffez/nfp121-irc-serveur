package model.dao.bean;
import java.sql.Date;

public class Log {
	private int idLog;
	private String user_idUser; // Mis en string pour pouvoir renvoyer une valeur null
	private String instruction;
	private int code;
	private String message;
	private Date date;
	
	public int getIdLog() {
		return idLog;
	}
	public void setIdLog(int idLog) {
		this.idLog = idLog;
	}
	public String getInstruction() {
		return instruction;
	}
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getUser_idUser() {
		return user_idUser;
	}
	public void setUser_idUser(String user_idUser) {
		this.user_idUser = user_idUser;
	}

}
