package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import model.dao.bean.Message;



public class DefaultDAOMessage extends AbstractDAO implements DAOMessage{

	protected DefaultDAOMessage(Connection connect) {
		super(connect);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Message find(Object Id) throws DAOException {
		final String sql = "SELECT * FROM `message` WHERE `idMessage` = ?";
		if(!(Id instanceof Integer)) {
			throw new DAOException("The ID isn't an Integer.");
		}
		PreparedStatement st = null;
		ResultSet r = null;
		
		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, (int) Id);
			r = st.executeQuery();
			
			if(r.next()) {
				final Message m = new Message();
				m.setIdMessage(r.getInt("IdMessage"));
				m.setUser_idUser(r.getInt("User_idUser"));
				m.setChannel(r.getString("channel"));
				m.setMessage(r.getString("message"));
				m.setDate(r.getDate("date"));
				return m;
				
			}
			throw new DAOException("Message not found.");

			
		} catch (Exception e) {
			throw new DAOException("Error during loading message from the database", e);
		} finally {
			DAOUtils.close(r,st);
		}
	}

	@Override
	public Message create(Message obj) throws DAOException {
				final String sql = "INSERT INTO `message` VALUES (NULL, ?, ?, ?, CURRENT_TIMESTAMP)";

				PreparedStatement st = null;
				ResultSet rs = null;
				int r;

				try {
					st = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					st.setInt(1, obj.getUser_idUser());
					st.setString(2, obj.getChannel());
					st.setString(3, obj.getMessage());
					r = st.executeUpdate();
					rs = st.getGeneratedKeys();
					
					if(r > 0 && rs.next()) {
						obj.setIdMessage(rs.getInt(1));
						return obj;
					}
					
					throw new DAOException("Error during message insert in the database");
					
				} catch (SQLException e) {
					throw new DAOException("Error during creating message in the database", e);
				} finally {
					DAOUtils.close(st);
				}
			
			}
	

	@Override
	public Message update(Message obj) throws DAOException {
		if(this.find(obj.getIdMessage()) == null){
			throw new DAOException ("Message don't exist in database");
		}
		final String sql="UPDATE `message` SET `User_idUser` =?, `channel` =?, `message` =?, `date` =? WHERE `idMessage` =?";
		PreparedStatement st = null;
		int r;
		
		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, obj.getUser_idUser());
			st.setString(2,  obj.getChannel());
			st.setString(3, obj.getMessage());
			st.setDate(4, obj.getDate());
			st.setInt(5, obj.getIdMessage());
			
			r = st.executeUpdate();
			
			if(r > 0) {
				return obj;
			}
			throw new DAOException("Error during sql query");
		}
		catch(Exception e) {
			throw new DAOException ("Message not updated" + e);
		}
		finally {
			DAOUtils.close(st);
		}
	}
	

	@Override
	public void delete(Message obj) throws DAOException {
		final String sql = "DELETE FROM `message` WHERE `idMessage`=?";

		PreparedStatement st = null;
		int r;

		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, obj.getIdMessage());
			r = st.executeUpdate();

			if (r == 0) {
				throw new DAOException("Message not found.");
			}

		} catch (SQLException e) {
			throw new DAOException("Error during deleting message from the database", e);
		} finally {
			DAOUtils.close(st);
		}

	}

	@Override
	public List<Message> List() throws DAOException {
		final String sql = "SELECT * FROM `message`";
		final List<Message> messageList = new LinkedList<>();
		PreparedStatement st = null;
		ResultSet r = null;

		try {
			st = connect.prepareStatement(sql);
			r = st.executeQuery();

			while (r.next()) {
				final Message m = new Message();
				m.setIdMessage(r.getInt("idMessage"));
				m.setUser_idUser(r.getInt("User_idUser"));
				m.setChannel(r.getString("channel"));
				m.setMessage(r.getString("message"));
				m.setDate(r.getDate("date"));
				messageList.add(m);
			}
			
			return messageList;	

			
		} catch (SQLException e) {
			throw new DAOException("Error during loading message from the database", e);
		} finally {
			DAOUtils.close(r, st);
		}

	}

	@Override
	public Message getMessageFromChannelName(String channelname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getAllChannel() throws DAOException {
		final String sql = "SELECT DISTINCT `channel` FROM `message`";
		PreparedStatement st = null;
		ResultSet r = null;
		final List<String> channelList = new LinkedList<>();
		
		try {
			st = connect.prepareStatement(sql);
			r = st.executeQuery();
			
			if(r.next()) {
				do {
				 String u = new String();
				u = (r.getString("channel"));
				channelList.add(u);
				}
				while(r.next());
				return channelList;
			}
			throw new DAOException ("No channel in database.");	
			
		} catch (Exception e) {
			throw new DAOException("Error during loading channel from database");
		}
		finally {
			DAOUtils.close(r,st);
		}
	}

	@Override
	public List<String> getMessagesAndDatesFromUser(String search, int id) throws DAOException {
		final String sql ="SELECT `message`,`date`,`channel`,`login` FROM message INNER JOIN user ON message.User_iduser = user.iduser WHERE `User_iduser`=? AND `message` LIKE ?";
		PreparedStatement st = null;
		ResultSet r = null;
		final List<String> messageList = new LinkedList<>();
		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, id);
			st.setString(2,  "%" + search + "%");
			r = st.executeQuery();
			
			if(r.next()) {
				do {
				String m = new String();
				m = (r.getString("date") + " [" + r.getString("channel") + "] " + r.getString("login") + " : " + r.getString("message"));
				messageList.add(m);
				}
				while(r.next());
				return messageList;
			}
			throw new DAOException ("No message for this search?");	
		} catch (Exception e) {
			throw new DAOException("Error during loading message from database", e);
		}
		finally {
			DAOUtils.close(r,st);
		}

	}

	@Override
	public List<String> getMessagesFromChannel(String search, String channel) throws DAOException {
		final String sql ="SELECT `message`,`date`,`channel`,`login` FROM message INNER JOIN user ON message.User_iduser = user.iduser WHERE `channel`=? AND `message` LIKE ?";
		PreparedStatement st = null;
		ResultSet r = null;
		final List<String> messageList = new LinkedList<>();
		try {
			st = connect.prepareStatement(sql);
			st.setString(1, channel);
			st.setString(2,  "%" + search + "%");
			r = st.executeQuery();
			
			if(r.next()) {
				do {
				String m = new String();
				m = (r.getString("date") + " [" + r.getString("channel") + "] " + r.getString("login") + " : " + r.getString("message"));
				messageList.add(m);
				}
				while(r.next());
				return messageList;
			}
			throw new DAOException ("No message for this search");	
		} catch (Exception e) {
			throw new DAOException("Error during loading message from database", e);
		}
		finally {
			DAOUtils.close(r,st);
		}

	}

	@Override
	public List<String> getLastMessages() throws DAOException {
		final String sql ="SELECT `message`,`date`,`channel`,`login` FROM message INNER JOIN user ON message.User_iduser = user.iduser ORDER BY idmessage ASC LIMIT 14";
		PreparedStatement st = null;
		ResultSet r = null;
		final List<String> messageList = new LinkedList<>();
		try {
			st = connect.prepareStatement(sql);
			r = st.executeQuery();
			
			if(r.next()) {
				do {
				String m = new String();
				m = (r.getString("date") + " [" + r.getString("channel") + "] " + r.getString("login") + " : " + r.getString("message"));
				messageList.add(m);
				}
				while(r.next());
				return messageList;
			}
			throw new DAOException ("No messages in database");	
		} catch (Exception e) {
			throw new DAOException("Error during loading message from database", e);
		}
		finally {
			DAOUtils.close(r,st);
		}
	}
		



}
