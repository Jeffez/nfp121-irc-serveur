package model.dao;

import model.dao.bean.Message;
import java.util.List;

public interface DAOMessage extends DAO<Message> {
	public Message getMessageFromChannelName(String channelname);
	public List<String> getAllChannel() throws DAOException;
	public List<String> getMessagesAndDatesFromUser(String search, int id) throws DAOException;
	public List<String> getMessagesFromChannel(String search, String channel) throws DAOException;
	public List<String> getLastMessages() throws DAOException;
}
