package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import model.dao.bean.Log;
import model.dao.bean.Message;

public class DefaultDAOLog extends AbstractDAO implements DAOLog{

	protected DefaultDAOLog(Connection connect) {
		super(connect);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Log find(Object Id) throws DAOException {
		final String sql = "SELECT * FROM `message` WHERE `idlog` = ?";
		if(!(Id instanceof Integer)) {
			throw new DAOException("The ID isn't an Integer.");
		}
		PreparedStatement st = null;
		ResultSet r = null;
		
		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, (int) Id);
			r = st.executeQuery();
			
			if(r.next()) {
				final Log l = new Log();
				l.setIdLog(r.getInt("idlog"));
				l.setUser_idUser(r.getString("User_idUser"));
				l.setInstruction(r.getString("instruction"));
				l.setCode(r.getInt("code"));
				l.setMessage(r.getString("message"));
				l.setDate(r.getDate("date"));
				return l;
				
			}
			throw new DAOException("Log not found.");

			
		} catch (Exception e) {
			throw new DAOException("Error during loading log from the database", e);
		} finally {
			DAOUtils.close(r,st);
		}
	}

	@Override
	public Log create(Log obj) throws DAOException {
		final String sql = "INSERT INTO `log` VALUES (NULL, ?, ?, ?, ?, CURRENT_TIMESTAMP)";

		PreparedStatement st = null;
		ResultSet rs = null;
		int r;

		try {
			st = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			st.setString(1, obj.getUser_idUser());
			st.setString(2, obj.getInstruction());
			st.setInt(3, obj.getCode());
			st.setString(4, obj.getMessage());
			r = st.executeUpdate();
			rs = st.getGeneratedKeys();
			
			if(r > 0 && rs.next()) {
				obj.setIdLog(rs.getInt(1));
				return obj;
			}
			
			throw new DAOException("Error during log insert in the database");
			
		} catch (SQLException e) {
			throw new DAOException("Error during creating log in the database", e);
		} finally {
			DAOUtils.close(st);
		}
	
	}

	@Override
	public Log update(Log obj) throws DAOException {
		if(this.find(obj.getIdLog()) == null){
			throw new DAOException ("Log don't exist in database");
		}
		final String sql="UPDATE `log` SET `User_idUser` =?, `instruction` =?, `code` =?, `message` =?, `date` =? WHERE `idlog` =?";
		PreparedStatement st = null;
		int r;
		
		try {
			st = connect.prepareStatement(sql);
			st.setString(1, obj.getUser_idUser());
			st.setString(2,  obj.getInstruction());
			st.setInt(3, obj.getCode());
			st.setString(4, obj.getMessage());
			st.setDate(5, obj.getDate());
			
			r = st.executeUpdate();
			
			if(r > 0) {
				return obj;
			}
			throw new DAOException("Error during sql query");
		}
		catch(Exception e) {
			throw new DAOException ("Log not updated" + e);
		}
		finally {
			DAOUtils.close(st);
		}
	}

	@Override
	public void delete(Log obj) throws DAOException {
		final String sql = "DELETE FROM `log` WHERE `idlog`=?";

		PreparedStatement st = null;
		int r;

		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, obj.getIdLog());
			r = st.executeUpdate();

			if (r == 0) {
				throw new DAOException("Log not found.");
			}

		} catch (SQLException e) {
			throw new DAOException("Error during deleting log from the database", e);
		} finally {
			DAOUtils.close(st);
		}
		
	}

	@Override
	public List<Log> List() throws DAOException {
		final String sql = "SELECT * FROM `log`";
		final List<Log> logList = new LinkedList<>();
		PreparedStatement st = null;
		ResultSet r = null;

		try {
			st = connect.prepareStatement(sql);
			r = st.executeQuery();

			while (r.next()) {
				final Log l = new Log();
				l.setIdLog(r.getInt("idlog"));
				l.setUser_idUser(r.getString("User_idUser"));
				l.setInstruction(r.getString("instruction"));
				l.setCode(r.getInt("code"));
				l.setMessage(r.getString("message"));
				l.setDate(r.getDate("date"));
				logList.add(l);
			}
			
			return logList;	

			
		} catch (SQLException e) {
			throw new DAOException("Error during loading message from the database", e);
		} finally {
			DAOUtils.close(r, st);
		}
	}

}
