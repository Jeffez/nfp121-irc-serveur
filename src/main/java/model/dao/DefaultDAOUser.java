package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.LinkedList;

import model.dao.bean.User;

public class DefaultDAOUser extends AbstractDAO implements DAOUser{

	protected DefaultDAOUser(Connection connect) {
		super(connect);
		// TODO Auto-generated constructor stub
	}

	@Override
	public User find(Object Id) throws DAOException {
		final String sql = "SELECT * FROM `user` WHERE `idUser` = ?";
		if(!(Id instanceof Integer)) {
			throw new DAOException("The ID isn't an Integer.");
		}
		PreparedStatement st = null;
		ResultSet r = null;
		
		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, (int) Id);
			r = st.executeQuery();
			
			if(r.next()) {
				final User u = new User();
				u.setIdUser(r.getInt("IdUser"));
				u.setLogin(r.getString("login"));
				u.setPassword(r.getString("password"));
				return u;
				
			}
			throw new DAOException("User not found.");

			
		} catch (Exception e) {
			throw new DAOException("Error during loading user from the database", e);
		} finally {
			DAOUtils.close(r,st);
		}
	}

	@Override
	public User create(User obj) throws DAOException {
		// Cas où l'utilisateur existe déjà dans la base.
		if(this.userExist(obj.getLogin()))
		{
			throw new DAOException ("User already exist in database");
		}
		final String sql = "INSERT INTO `user` VALUES (NULL, ?, ?)";

		PreparedStatement st = null;
		ResultSet rs = null;
		int r;

		try {
			st = connect.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			st.setString(1, obj.getLogin());
			st.setString(2, obj.getPassword());
			r = st.executeUpdate();
			rs = st.getGeneratedKeys();
			
			if(r > 0 && rs.next()) {
				obj.setIdUser(rs.getInt(1));
				return obj;
			}
			
			throw new DAOException("Error during user insert in the database");
			
		} catch (SQLException e) {
			throw new DAOException("Error during creating user in the database", e);
		} finally {
			DAOUtils.close(st);
		}
	
	}

	@Override
	public User update(User obj) throws DAOException {
		if(this.find(obj.getIdUser()) == null){
			throw new DAOException ("User don't exist in database");
		}
		final String sql="UPDATE `user` SET `login`=?,`password`=? WHERE `idUser`=?";
		PreparedStatement st = null;
		final int r;
		
		try {
			st = connect.prepareStatement(sql);
			st.setString(1, obj.getLogin());
			st.setString(2,  obj.getPassword());
			st.setInt(3, obj.getIdUser());
			r = st.executeUpdate();
			
			if(r > 0) {
				return obj;
			}
			throw new DAOException("Error during sql query");
		}
		catch(Exception e) {
			throw new DAOException ("User not updated" + e);
		}  finally {
			DAOUtils.close(st);
		}
	}

	@Override
	public void delete(User obj) throws DAOException {
		final String sql="DELETE FROM `user` WHERE `idUser`=?";
		PreparedStatement st = null;
		final int r;
		
		try {
			st = connect.prepareStatement(sql);
			st.setInt(1, obj.getIdUser());
			r = st.executeUpdate();
			
			if(r == 0) {
				throw new DAOException("User not found");
			}
		}
		catch(Exception e) {
			throw new DAOException ("User not deleted" + e);
		}
		finally {
			DAOUtils.close(st);
		}
		
	}

	@Override
	public List<User> List() throws DAOException {
		final String sql = "SELECT * FROM `user`";
		PreparedStatement st = null;
		ResultSet r = null;
		final List<User> userList = new LinkedList<>();
		
		try {
			st = connect.prepareStatement(sql);
			r = st.executeQuery();
			
			if(r.next()) {
				do {
				final User u = new User();
				u.setIdUser(r.getInt("Id"));
				u.setLogin(r.getString("login"));
				u.setPassword(r.getString("password"));
				userList.add(u);
				}
				while(r.next());
				return userList;
			}
			throw new DAOException ("No users in database.");	
			
		} catch (Exception e) {
			throw new DAOException("Error during loading user from database");
		}
		finally {
			DAOUtils.close(r,st);
		}
	}

	@Override
	public User getuserFromLoginAndPassword(String login, String password) throws DAOException {
		if(!login.isEmpty() || !password.isEmpty()) {
		final String sql = "SELECT * FROM `user` WHERE `login`=? AND `password`=?";
		PreparedStatement st = null;
		ResultSet r = null;
		
		try {
			st = connect.prepareStatement(sql);
			st.setString(1, login);
			st.setString(2,  password);
			r = st.executeQuery();
			
			if(r.next()) {
				User u = new User();
				u.setIdUser(r.getInt("IdUser"));
				u.setLogin(r.getString("login"));
				u.setPassword(r.getString("password"));
				return u;
				
			}
		
		} catch (Exception e) {
			throw new DAOException ("User not found. " + e);
		}
		finally {
			DAOUtils.close(r,st);
		}
		}
		throw new DAOException("User or password are missing");
		
	}

	@Override
	public List<String> getAllUsers() throws DAOException {
		final String sql = "SELECT `login` FROM `user`";
		PreparedStatement st = null;
		ResultSet r = null;
		final List<String> userList = new LinkedList<>();
		
		try {
			st = connect.prepareStatement(sql);
			r = st.executeQuery();
			
			if(r.next()) {
				do {
				 String u = new String();
				u = (r.getString("login"));
				userList.add(u);
				}
				while(r.next());
				return userList;
			}
			throw new DAOException ("No users in database.");	
			
		} catch (Exception e) {
			throw new DAOException("Error during loading user from database");
		}
		finally {
			DAOUtils.close(r,st);
		}
	}

	@Override
	public User getUserFromLogin(String login) throws DAOException {

		if(login.isEmpty()) {
			throw new DAOException("User field is empty.");
		}
			final String sql = "SELECT * FROM `user` WHERE `login`=?";
			PreparedStatement st = null;
			ResultSet r = null;
			
			try {
				st = connect.prepareStatement(sql);
				st.setString(1, login);
				r = st.executeQuery();
				
				
				if(r.next()) {
					User u = new User();
					u.setIdUser(r.getInt("IdUser"));
					u.setLogin(r.getString("login"));
					u.setPassword(r.getString("password"));
					return u;
					
				}
			
			} catch (Exception e) {
				throw new DAOException ("Error during loading user from database " + e);
			}
			finally {
				DAOUtils.close(r,st);
			}
			throw new DAOException("User not found");
			
		}
	
	

	@Override
	public int getIdFromLogin(String login) throws DAOException {
		if(!login.isEmpty()) {
			final String sql = "SELECT `idUser` FROM `user` WHERE `login`=?";
			PreparedStatement st = null;
			ResultSet r = null;
			
			try {
				st = connect.prepareStatement(sql);
				st.setString(1, login);
				r = st.executeQuery();
				
				if(r.next()) {
					return r.getInt("IdUser");
					
				}
			
			} catch (Exception e) {
				throw new DAOException ("User not found. " + e);
			}
			finally {
				DAOUtils.close(r,st);
			}
			}
			throw new DAOException("User field is empty.");
	}

	@Override
	public boolean userExist(String login) throws DAOException {
			final String sql = "SELECT * FROM `user` WHERE `login`=?";
			PreparedStatement st = null;
			ResultSet r = null;
			
			try {
				st = connect.prepareStatement(sql);
				st.setString(1, login);
				r = st.executeQuery();
				
				if(r.next()) {
					return true;
					
				}
			
			} catch (Exception e) {

				throw new DAOException ("User not found. " + e);
				
			}
			finally {
				DAOUtils.close(r,st);
			}
			return false;
		}
}
	


