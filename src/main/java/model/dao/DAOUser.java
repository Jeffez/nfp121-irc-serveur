package model.dao;

import model.dao.bean.User;
import java.util.List;

public interface DAOUser extends DAO<User> {
	public User getuserFromLoginAndPassword (String login, String password) throws DAOException;
	public List<String> getAllUsers() throws DAOException;
	public User getUserFromLogin(String login) throws DAOException;
	public int getIdFromLogin(String login) throws DAOException;
	public boolean userExist(String login) throws DAOException;
}
