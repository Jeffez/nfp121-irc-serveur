package log;

import org.apache.log4j.Logger;

import app.Main;
import communication.SocketListenner;
import model.dao.DAOException;
import model.dao.DAOFactory;
import model.dao.DAOLog;
import model.dao.bean.Log;

public class LogIRC {

	private static final Logger LOG = Logger.getLogger(SocketListenner.class.getName());

	// INFO

	public static void info(String message) {

		info(message, null, null, 0);
	}

	public static void info(String message, String login) {

		info(message, login, null, 0);
	}

	public static void info(String message, String login, String instruction, int code) {

		LOG.info(login + " - " + message);
		Main.mainFrame.newLine("[INFO] - (" + login + ") - " + message);
		saveDatabase(message, login, code, instruction);
	}

	// ERROR

	public static void error(String message, Exception e) {

		error(message, e, null, null, 0);
	}

	public static void error(String message, Exception e, String login) {

		error(message, e, login, null, 0);
	}

	public static void error(String message, Exception e, String login, String instruction, int code) {

		LOG.error(login + " - " + message, e);
		Main.mainFrame.newLine("[ERROR] - (" + login + ") - " + message);
		saveDatabase(message, login, code, instruction);
	}

	// SAVE DATABASE

	private static void saveDatabase(String message, String login, int code, String instruction) {

		//HotFix waiting Login can be nullify
		
		try {
			Log log = new Log();
			log.setMessage(message);
			if (instruction != null) {
				log.setInstruction(instruction);
			}
			if (code != 0) {
				log.setCode(code);
			}
			// GET USER ID and SET it
			if (login != null) {
				log.setUser_idUser(Main.daoUser.getIdFromLogin(login)+"");
			}
			Main.daoLog.create(log);
			
		} catch (DAOException e) {
			LOG.error("Error DAO, cannot save log", e);
			return;
		}

	}
}
