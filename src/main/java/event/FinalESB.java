package event;

/**
 * 
 * @author Julien-Erwan
 *
 */
public class FinalESB {

	// Informative Message
	public static final EventServerBuilder	USER_LIST			= new EventServerBuilder().setCode(110);
	public static final EventServerBuilder	CANAL_LIST			= new EventServerBuilder().setCode(120);
	public static final EventServerBuilder	MESSAGE				= new EventServerBuilder().setCode(130);

	// Success Message
	public static final EventServerBuilder	SUCCESS				= new EventServerBuilder().setCode(200).addData("message", "Success");

	// Failed Message
	public static final EventServerBuilder	LOGIN_IN_USE		= new EventServerBuilder().setCode(310).addData("message", "Login already in use");
	public static final EventServerBuilder	INVALID_PASSWORD	= new EventServerBuilder().setCode(311).addData("message", "Invalid password");
	public static final EventServerBuilder	USER_NOT_CONNECTED	= new EventServerBuilder().setCode(312).addData("message", "User not connected");
	public static final EventServerBuilder	LOGIN_TOO_LONG		= new EventServerBuilder().setCode(313).addData("message", "Password/Login too long");
	public static final EventServerBuilder	DIDNT_JOIN_CANAL	= new EventServerBuilder().setCode(320).addData("message",
			"User didn't join a canal");

	// Error Message
	public static final EventServerBuilder	WRONG_CANAL			= new EventServerBuilder().setCode(420).addData("message", "Wrong canal");
	public static final EventServerBuilder	NO_CANAL			= new EventServerBuilder().setCode(421).addData("message", "No canal");
	public static final EventServerBuilder	NONEXISTENT_CANAL	= new EventServerBuilder().setCode(422).addData("message", "Nonexistent canal");
	public static final EventServerBuilder	MESSAGE_TOO_LONG	= new EventServerBuilder().setCode(430).addData("message", "Message too long");
	public static final EventServerBuilder	EMPTY_MESSAGE		= new EventServerBuilder().setCode(431).addData("message", "Emtpy Message");

	// Other Message
	public static final EventServerBuilder	UNKNOWN_ERROR		= new EventServerBuilder().setCode(000).addData("message", "Unknown Error");
	public static final EventServerBuilder	INVALID_JSON		= new EventServerBuilder().setCode(001).addData("message", "Couldn't Parse JSON");
	public static final EventServerBuilder	UNKNOWN_INSTRUCTION	= new EventServerBuilder().setCode(002).addData("message", "Unknown Instruction");

}
