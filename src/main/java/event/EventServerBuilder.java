package event;

import org.json.JSONObject;

/**
 * Build a JSON Event
 * 
 * @author Julien-Erwan
 *
 */
public class EventServerBuilder {

	private JSONObject buildedJSON;

	public EventServerBuilder() {

		buildedJSON = new JSONObject();
	}

	public EventServerBuilder setCode(int code) {

		buildedJSON.put("code", code);
		return this;
	}

	public EventServerBuilder addData(String dataName, String value) {

		buildedJSON.put(dataName, value);
		return this;
	}

	public EventServerBuilder addDataArray(String dataName, String[] dataArray) {

		buildedJSON.put(dataName, dataArray);
		return this;
	}

	public JSONObject build() {

		return this.buildedJSON;
	}
}
