package event.client;

import java.net.Socket;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;

/**
 * Subscribe to a Channel
 * 
 * @author Julien-Erwan
 *
 */
public class SubscribeEvent extends DefaultEvent {

	public SubscribeEvent(JSONObject json, Socket client) {

		super(json, client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");
			String channel = json.getString("channel");
			String target_channel = json.getString("target_channel");

			// Test Connexion
			if (!SocketSpeaker.USERS.containsKey(login) || !SocketSpeaker.USERS.get(login).equals(client)) {
				SocketSpeaker.sendToClient(FinalESB.USER_NOT_CONNECTED.build(), client);
				return;
			}

			// Test if the user is in the requested Channel
			if (
				(SocketSpeaker.USER_BY_CHANNEL.containsKey(channel) && !SocketSpeaker.USER_BY_CHANNEL.get(channel).containsKey(login))
						|| !SocketSpeaker.USER_BY_CHANNEL.containsKey(channel)
			) {
				SocketSpeaker.sendToClient(FinalESB.WRONG_CANAL.build(), client);
				return;
			}

			// Remove User from Channel and delete channel if Empty
			SocketSpeaker.USER_BY_CHANNEL.get(channel).remove(login);
			if (SocketSpeaker.USER_BY_CHANNEL.get(channel).isEmpty()) {
				SocketSpeaker.USER_BY_CHANNEL.remove(channel);
			}

			// Create New Channel if nonexistent
			if (!SocketSpeaker.USER_BY_CHANNEL.containsKey(target_channel)) {
				SocketSpeaker.USER_BY_CHANNEL.put(target_channel, new HashMap<String, Socket>());
			}

			// Add user to the new channel
			SocketSpeaker.USER_BY_CHANNEL.get(target_channel).put(login, client);

			// Send Success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);
			return;
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		}
	}

	@Override
	public void log() {

		LogIRC.info("Try to subscribe to a channel");
	}

}
