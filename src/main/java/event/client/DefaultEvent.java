package event.client;

import java.net.Socket;

import org.json.JSONObject;

/**
 * Abstract Event Class
 * 
 * @author Julien-Erwan
 *
 */
public abstract class DefaultEvent {

	protected JSONObject	json;
	protected Socket		client;

	public DefaultEvent(JSONObject json, Socket client) {

		this.json = json;
		this.client = client;
	}

	public abstract void action();

	public abstract void log();

}
