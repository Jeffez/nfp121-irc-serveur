package event.client;

import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;

/**
 * List all Channel available
 * 
 * @author Julien-Erwan
 *
 */
public class CanalListEvent extends DefaultEvent {

	public CanalListEvent(JSONObject json, Socket client) {

		super(json, client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");

			// Test Connexion
			if (!SocketSpeaker.USERS.containsKey(login) || !SocketSpeaker.USERS.get(login).equals(client)) {
				SocketSpeaker.sendToClient(FinalESB.USER_NOT_CONNECTED.build(), client);
				return;
			}

			// Test if there isn't any Channel
			if (SocketSpeaker.USER_BY_CHANNEL.isEmpty()) {
				SocketSpeaker.sendToClient(FinalESB.NO_CANAL.build(), client);
				return;
			}

			// Send Success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);

			// Send Channel List
			SocketSpeaker.sendToClient(
					FinalESB.CANAL_LIST.addDataArray("all_channel", SocketSpeaker.USER_BY_CHANNEL.keySet().toArray(new String[0])).build(), client);
			return;
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		}
	}

	@Override
	public void log() {

		LogIRC.info("Try to get the lis of Canal");

	}

}
