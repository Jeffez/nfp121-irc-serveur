package event.client;

import java.io.IOException;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;

/**
 * Deconnexion to the server
 * 
 * @author Julien-Erwan
 *
 */
public class DeconnexionEvent extends DefaultEvent {

	public DeconnexionEvent(JSONObject json, Socket client) {

		super(json, client);
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");

			// Test Connexion
			if (!SocketSpeaker.USERS.containsKey(login)) {
				SocketSpeaker.sendToClient(FinalESB.USER_NOT_CONNECTED.build(), client);
				return;
			}

			// Remove from the active users
			SocketSpeaker.removeUser(login);

			// Send success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);
			client.close();
			return;
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		} catch (IOException e) {
			LogIRC.error("Cannot close socket", e);
			SocketSpeaker.sendToClient(FinalESB.UNKNOWN_ERROR.build(), client);
			return;
		}

	}

	@Override
	public void log() {

		LogIRC.info("Try a deconnexion");

	}

}
