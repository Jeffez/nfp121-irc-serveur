package event.client;

import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import app.Main;
import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;
import model.dao.DAOException;
import model.dao.DAOFactory;
import model.dao.DAOUser;
import model.dao.bean.User;

/**
 * Connexion to the server
 * 
 * @author Julien-Erwan
 *
 */
public class ConnexionEvent extends DefaultEvent {

	public ConnexionEvent(JSONObject json, Socket client) {

		super(json, client);
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");
			String password = json.getString("password");
			// Test if login and password are Empty
			if (login.isEmpty() || password.isEmpty()) {
				SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
				return;
			}

			// Test Size Login and Password
			if (login.length() > 45 || password.length() > 45) {
				SocketSpeaker.sendToClient(FinalESB.LOGIN_TOO_LONG.build(), client);
				return;
			}

			// Test if the user is already connected
			if (SocketSpeaker.USERS.containsKey(login)) {
				SocketSpeaker.sendToClient(FinalESB.LOGIN_IN_USE.build(), client);
				return;
			}

			try {
				User u = Main.daoUser.getUserFromLogin(login);
				// Test if password isn't correct
				if (!u.getPassword().equals(password)) {
					SocketSpeaker.sendToClient(FinalESB.INVALID_PASSWORD.build(), client);
					return;
				}
			} catch (DAOException e) {
				// DAOException User not found. Create new User
				LogIRC.info("User not found, creation of new User");
				try {
					User u = new User();
					u.setLogin(login);
					u.setPassword(password);
					Main.daoUser.create(u);
				} catch (DAOException er) {
					LogIRC.error("Error DAO during User Creation", er);
					SocketSpeaker.sendToClient(FinalESB.UNKNOWN_ERROR.build(), client);
					return;
				}
			}
			// Add the user in the list of already connected User
			SocketSpeaker.USERS.put(login, client);
			// Add the user to the default channel
			SocketSpeaker.USER_BY_CHANNEL.get("default").put(login, client);

			// Send Success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);

			return;
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		}
	}

	@Override
	public void log() {

		LogIRC.info("Try a connexion");
	}

}
