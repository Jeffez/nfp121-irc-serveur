package event.client;

import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;

/**
 * 
 * 
 * @author Julien-Erwan
 *
 */
public class EventParser {

	public static DefaultEvent parse(String message, Socket client) {

		DefaultEvent event = null;
		try {
			JSONObject json = new JSONObject(new JSONTokener(message));
			// Parse the JSON Object
			switch(json.get("instruction").toString()) {
				
				case "connect":
					event = new ConnexionEvent(json, client);
					break;
				case "disconnect":
					event = new DeconnexionEvent(json,client);
					break;
				case "list_all_members":
					event = new UserListEvent(json,client);
					break;
				case "list_channels":
					event = new CanalListEvent(json,client);
					break;
				case "list_channel_members":
					event = new MemberListEvent(json,client);
					break;
				case "subscribe_channel":
					event = new SubscribeEvent(json,client);
					break;
				case "send_message":
					event = new MessageEvent(json,client);
					break;
				default:
					SocketSpeaker.sendToClient(FinalESB.UNKNOWN_INSTRUCTION.build(),client);
					break;
			}
		}catch(JSONException e) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(),client);
			return null;
		}
		
		if(event == null) {
			LogIRC.error("Creation of a null Event", new NullPointerException());
			SocketSpeaker.sendToClient(FinalESB.UNKNOWN_ERROR.build(),client);
		}
		
		return event;
	}

}
