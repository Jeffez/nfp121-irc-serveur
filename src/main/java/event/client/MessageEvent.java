package event.client;

import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import app.Main;
import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;
import model.dao.DAOException;
import model.dao.DAOFactory;
import model.dao.DAOMessage;
import model.dao.bean.Message;

/**
 * Send a Message
 * 
 * @author Julien-Erwan
 *
 */
public class MessageEvent extends DefaultEvent {

	private final int MAX_SIZE = 500;

	public MessageEvent(JSONObject json, Socket client) {

		super(json, client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");
			String channel = json.getString("channel");
			String message = json.getString("message");

			// Test Connexion
			if (!SocketSpeaker.USERS.containsKey(login) || !SocketSpeaker.USERS.get(login).equals(client)) {
				SocketSpeaker.sendToClient(FinalESB.USER_NOT_CONNECTED.build(), client);
				return;
			}

			// Test if the User is in the Requested Channel
			if (
				(SocketSpeaker.USER_BY_CHANNEL.containsKey(channel) && !SocketSpeaker.USER_BY_CHANNEL.get(channel).containsKey(login))
						|| !SocketSpeaker.USER_BY_CHANNEL.containsKey(channel)
			) {
				SocketSpeaker.sendToClient(FinalESB.WRONG_CANAL.build(), client);
				return;
			}

			// Test if the Message is Empty
			if (message == null || message.isEmpty()) {
				SocketSpeaker.sendToClient(FinalESB.EMPTY_MESSAGE.build(), client);
				return;
			}

			// Test if the Message doesn't exceed the maximum size
			if (message.length() > MAX_SIZE) {
				SocketSpeaker.sendToClient(FinalESB.MESSAGE_TOO_LONG.build(), client);
				return;
			}

			// Save Message into Database
			try {

				Message msg = new Message();
				msg.setChannel(channel);
				msg.setMessage(message);
				// GET USER ID and SET it
				msg.setUser_idUser(Main.daoUser.getIdFromLogin(login));

				Main.daoMessage.create(msg);
			} catch (DAOException e) {
				LogIRC.error("DAO Error, cannot save message", e);
				SocketSpeaker.sendToClient(FinalESB.UNKNOWN_ERROR.build(), client);
				return;
			}

			if(null != Main.exploitFrame) {
				Main.exploitFrame.newLine("[" + channel + "] " + login + " : " + message);
			}
			// Send Success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);

			// Send Message to all user in Channel
			SocketSpeaker.send(FinalESB.MESSAGE.addData("user", login).addData("message", message).addData("channel", channel).build(), channel);
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		}
	}

	@Override
	public void log() {

		LogIRC.info("Try to send a message");

	}

}
