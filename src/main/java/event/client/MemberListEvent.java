package event.client;

import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;

/**
 * 
 * List all User connected to a specific channel
 * 
 * @author Julien-Erwan
 *
 */
public class MemberListEvent extends DefaultEvent {

	public MemberListEvent(JSONObject json, Socket client) {

		super(json, client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");
			String channel = json.getString("channel");

			// Test Connexion
			if (!SocketSpeaker.USERS.containsKey(login) || !SocketSpeaker.USERS.get(login).equals(client)) {
				SocketSpeaker.sendToClient(FinalESB.USER_NOT_CONNECTED.build(), client);
				return;
			}

			// Test if the Channel exist
			if (!SocketSpeaker.USER_BY_CHANNEL.containsKey(channel)) {
				SocketSpeaker.sendToClient(FinalESB.NONEXISTENT_CANAL.build(), client);
				return;
			}

			// Send Success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);

			// Send User List
			SocketSpeaker.sendToClient(FinalESB.USER_LIST
					.addDataArray("all_members", SocketSpeaker.USER_BY_CHANNEL.get(channel).keySet().toArray(new String[0])).build(), client);
			return;
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		}
	}

	@Override
	public void log() {

		LogIRC.info("Try to get the List of the Member of a Channel");
	}

}
