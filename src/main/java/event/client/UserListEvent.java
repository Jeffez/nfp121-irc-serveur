package event.client;

import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import communication.SocketSpeaker;
import event.FinalESB;
import log.LogIRC;

/**
 * 
 * List all Users
 * 
 * @author Julien-Erwan
 *
 */
public class UserListEvent extends DefaultEvent {

	public UserListEvent(JSONObject json, Socket client) {

		super(json, client);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action() {

		try {
			String login = json.getString("login");

			// Test connexion
			if (!SocketSpeaker.USERS.containsKey(login) || !SocketSpeaker.USERS.get(login).equals(client)) {
				SocketSpeaker.sendToClient(FinalESB.USER_NOT_CONNECTED.build(), client);
				return;
			}

			// Send Success
			SocketSpeaker.sendToClient(FinalESB.SUCCESS.build(), client);

			// Send List to Client
			SocketSpeaker.sendToClient(FinalESB.USER_LIST.addDataArray("all_members", SocketSpeaker.USERS.keySet().toArray(new String[0])).build(),
					client);
			return;
		} catch (JSONException jse) {
			SocketSpeaker.sendToClient(FinalESB.INVALID_JSON.build(), client);
			return;
		}
	}

	@Override
	public void log() {

		LogIRC.info("Try to get all users");
	}

}
