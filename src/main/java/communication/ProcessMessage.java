package communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ConcurrentModificationException;
import java.util.Map.Entry;

import event.client.DefaultEvent;
import event.client.EventParser;
import log.LogIRC;

/**
 * 
 * @author Julien-Erwan
 *
 */
public class ProcessMessage implements Runnable {

	private Socket client;

	public ProcessMessage(Socket client) {

		// Stock the client in order to be able to listen to his socket.
		this.client = client;
	}

	@Override
	public void run() {

		InputStream in = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		String ip = null;
		try {
			in = client.getInputStream();
			isr = new InputStreamReader(in, "UTF-8");
			br = new BufferedReader(isr);

			// Read the first line of the network stream
			String line = br.readLine();
			while (line != null) {

				if (client.isClosed()) {
					break;
				}
				String message = line;
				ip = client.getInetAddress().getHostAddress();
				// Log what's send to us
				LogIRC.info("{" + ip + "} : " + message);

				DefaultEvent event = EventParser.parse(message, client);
				try {
					// Log into the DB the message
					event.log();
					// Launch the execution for said message
					event.action();
				} catch (NullPointerException e) {
					LogIRC.error("Event not define", e);
				}
				line = br.readLine();
			}
		} catch (IOException e) {
			LogIRC.info("{" + ip + "} : " + "Fermeture de la socket.");
			// If the connexion is stopped without a "disconnect", this method remove the
			// User from the list of the connected people.
			try {
			for (Entry<String, Socket> entry : SocketSpeaker.USERS.entrySet()) {
				if (entry.getValue().equals(client)) {
					SocketSpeaker.removeUser(entry.getKey());
				}
			}
			}catch(ConcurrentModificationException cme) {
				LogIRC.error("Couldn't remove all users", cme);
			}
		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (isr != null) {
					isr.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
				LogIRC.error("Error during stream closing.", e);
			}
			LogIRC.info("{" + ip + "} : " + "Close connexion");
		}

	}

}
