package communication;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.json.JSONObject;

import log.LogIRC;

/**
 * 
 * @author Julien-Erwan
 *
 */
public class SocketSpeaker {

	public static final HashMap<String, Socket> USERS = new HashMap<String, Socket>();
	public static final HashMap<String, HashMap<String, Socket>> USER_BY_CHANNEL = new HashMap<String, HashMap<String, Socket>>();

	/**
	 * Send a message to all client in a channel
	 * 
	 * @param message
	 * @param channel
	 */
	public static void send(JSONObject message, String channel) {

		try {
			for (Socket client : USER_BY_CHANNEL.get(channel).values()) {
				if (!client.isClosed()) {
					sendToClient(message, client);
				}
			}
		} catch (ConcurrentModificationException e) {
			LogIRC.error("Error Concurrence", e);
		}
	}

	/**
	 * Send a message to a specific client
	 * 
	 * @param jsonObject
	 * @param client
	 */
	public static void sendToClient(JSONObject jsonObject, Socket client) {

		OutputStream out = null;
		OutputStreamWriter osw = null;
		PrintWriter pw = null;

		try {
			out = client.getOutputStream();
			osw = new OutputStreamWriter(out);
			pw = new PrintWriter(osw);

			pw.print(jsonObject.toString() + "\n");
			pw.flush();

			LogIRC.info("Send to client {" + client.getInetAddress().getHostAddress() + "} - " + jsonObject.toString());
		} catch (IOException e) {
			LogIRC.error("Error durign message sending.", e);
		}

	}

	public static void removeUser(String login) {

		USERS.remove(login);
		for (HashMap<String, Socket> channel : USER_BY_CHANNEL.values()) {
			channel.remove(login);
		}

		for (Entry<String, HashMap<String, Socket>> entry : USER_BY_CHANNEL.entrySet()) {
			if (entry.getValue().isEmpty()) {
				USER_BY_CHANNEL.remove(entry.getKey());
			}
		}
	}

}
