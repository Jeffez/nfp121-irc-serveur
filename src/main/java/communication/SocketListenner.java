package communication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import log.LogIRC;

/**
 * 
 * @author Julien-Erwan
 *
 */
public class SocketListenner implements Runnable {

	private static boolean	stop	= false;
	private ServerSocket	server	= null;

	@Override
	public void run() {

		server = null;

		LogIRC.info("Socket server started on the port : 4567");

		try {
			server = new ServerSocket(4567);
		} catch (IOException e) {
			LogIRC.error("Error during server creation", e);
			return;
		}
		try {
			// Bind server socket on the port 4567

			while (true) {

				final Socket client = server.accept();

				if (stop) {
					stop = false;
					client.close();
					break;
				}

				if (!SocketSpeaker.USER_BY_CHANNEL.containsKey("default")) {
					SocketSpeaker.USER_BY_CHANNEL.put("default", new HashMap<String, Socket>());
				}
				// Lauch thread to process the client
				new Thread(new ProcessMessage(client)).start();
			}
		} catch (IOException e) {
			LogIRC.error("Error during new socket listenning.", e);
		} finally {
			try {
				if (server != null) {
					server.close();
				}
			} catch (IOException e) {
				LogIRC.error("Error during stream closing.", e);
			}
		}
		LogIRC.info("Server Closed");
	}

	public void stop() {

		stop = true;
		try {
			this.server.close();
		} catch (IOException e) {
			LogIRC.error("Error during stream closing", e);
		}

		for (Socket socket : SocketSpeaker.USERS.values()) {
			try {
				socket.close();
			} catch (IOException e) {
				LogIRC.error("Error during stream closing", e);
			}
		}
		LogIRC.info("Closing Server ...");
	}
}
