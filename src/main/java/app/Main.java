package app;

import java.sql.Connection;

import model.dao.DAOException;
import model.dao.DAOFactory;
import model.dao.DAOLog;
import model.dao.DAOMessage;
import model.dao.DAOUser;

public class Main {

	public static ExploitationLogsFrame exploitFrame;
	
	public static MainMenuFrame mainFrame;

	public static DAOLog daoLog = null;
	public static DAOUser daoUser = null;
	public static DAOMessage daoMessage = null;
	
	public static void main(String[] args) {
		//System.out.println("Hola !");
		Connection c = null;

        try {
            c = DAOFactory.getConnection();
            daoLog = DAOFactory.getDAOLog(c);
            daoUser = DAOFactory.getDAOUser(c);
            daoMessage = DAOFactory.getDAOMessage(c);

        } catch (DAOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		mainFrame = new MainMenuFrame();
		
	}

}
