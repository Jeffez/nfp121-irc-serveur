package app;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.text.BadLocationException;

import communication.SocketListenner;
import log.LogIRC;

public class MainMenuFrame extends JFrame {

	private SocketListenner		server				= new SocketListenner();
	private Thread				threadServer;
	private static final long	serialVersionUID	= 1L;
	private JTextArea			textArea;

	public MainMenuFrame() {

		super("T'Chat IRC (v0.1 alpha) - Menu principal");

		// Change the size of the frame
		this.setSize(480, 500);
		this.setResizable(false);
		
		// Center frame in relation to the computer screen
		this.setLocationRelativeTo(null);

		// A click on the cross closes frame
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		textArea = new JTextArea();
		
		final JLabel tlabel = new JLabel("Interface d'administration du serveur IRC : ");
		final JButton btnLogs = new JButton("Consulter les logs");
		final JButton btnStart = new JButton("Lancer le serveur IRC");
		final JButton btnStop = new JButton("Arreter le serveur IRC");
		final JScrollPane scroll = new JScrollPane(textArea);
		final Container container = new Container();

		container.setSize(480, 500);
		
		// Position and add "tlabel" in the container
		tlabel.setBounds(15, 10, 400, 30);
		container.add(tlabel);

		// Position and add "btnLogs" in the container
		btnLogs.setBounds(135, 70, 200, 30);
		btnLogs.setBackground(Color.BLACK);
		btnLogs.setForeground(Color.WHITE);
		btnLogs.setBorderPainted(false);
		container.add(btnLogs);

		// Position and add "btnStart" in the container
		btnStart.setBounds(135, 120, 200, 30);
		btnStart.setBackground(Color.BLACK);
		btnStart.setForeground(Color.WHITE);
		btnStart.setBorderPainted(false);
		container.add(btnStart);

		// Position and add "btnStop" in the container
		btnStop.setBounds(135, 170, 200, 30);
		btnStop.setBackground(Color.BLACK);
		btnStop.setForeground(Color.WHITE);
		btnStop.setBorderPainted(false);
		container.add(btnStop);

		// Position and add "scroll" in the container
		scroll.setBounds(10, 250, 445, 160);
		final TitledBorder title;
		Border blackline;
		blackline = BorderFactory.createLineBorder(Color.black);
		title = BorderFactory.createTitledBorder(blackline, "Informations");
		scroll.setBorder(title);// title of JTextera
		scroll.setBackground(Color.WHITE);
		textArea.setEditable(false);
		container.add(scroll);
	
		// Add listener on the btn Logs
		btnLogs.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					Main.exploitFrame = new ExploitationLogsFrame();
				} catch (SQLException e1) {
					LogIRC.error("the frame can't be open", e1);
				}
			}
		});

		// Add listener on the btn Start
		btnStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				threadServer = new Thread(server);
				threadServer.start();
			}
		});

		// Add listener on the btn Stop
		btnStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				server.stop();
			}
		});

		this.add(container);// add container on frame
		this.getContentPane().setBackground(Color.WHITE);
		// Show Jframe
		this.setVisible(true);
		
	}

	public void newLine(String line) {

		if (textArea.getLineCount() > 8) {
			try {
				textArea.replaceRange("", 0, textArea.getLineEndOffset(0));
			} catch (BadLocationException e) {
				LogIRC.error("Error with new line", e);
			}
		}
		 Date  date = new Date();
		 SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 String dateNow = dateformat2.format(date);
		textArea.append(dateNow + " " +  line + '\n');
	}

	// draw the bottom line of the window
	void drawLines(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;
		g2d.draw(new Line2D.Float(5f, 460f, 600f, 460f));
	}

	public void paint(Graphics g) {

		super.paint(g);
		drawLines(g);
	}
}
